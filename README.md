This repository provides binary resources (audio, images, models) for KtnFramework.

---

![alt text](https://i.creativecommons.org/l/by/4.0/88x31.png "https://creativecommons.org/licenses/by/4.0/")

This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
